package io.cadena.essentials;

import java.io.File;
import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import io.cadena.models.BlockHeader;
import io.ipfs.multihash.Multihash;


@SuppressWarnings("rawtypes")
public class Database {
	//private Config config;
	public DB db;
	
	public ConcurrentMap<Long, BlockHeader> block_headers;
	public ConcurrentMap header_index;
	public ConcurrentMap<Long, Multihash> block_index;
	public ConcurrentMap alloc_set;

	public Database(File file) {
		this.db = DBMaker
				.fileDB(file)
				//.checksumStoreEnable()
				.closeOnJvmShutdown()
				.checksumHeaderBypass()
				.transactionEnable()
				.make();
		
		block_headers = db.hashMap("block_headers", Serializer.LONG, Serializer.JAVA).createOrOpen();
		
	}
 	public void commit() {
		this.db.commit();
	}
}
