package io.cadena.essentials;

import java.nio.ByteBuffer;
import java.util.List;

import io.cadena.models.Block;
import io.cadena.models.BlockHeader;
import io.ipfs.multihash.Multihash;

public interface Util {
	
	
	interface blockchain {
		static Block craftEmptyBlock(int height, List<Multihash> TransactionIds) {
			//BlockHeader header = new BlockHeader();
			//header.
		}
	}
    interface datahelper {
    	public static int difference(Multihash point1, Multihash point2) {
    		byte[] diff = xor(point1.toBytes(), point2.toBytes());
    		ByteBuffer wrapped = ByteBuffer.wrap(diff);
    		int intdiff = 0;
    		for(byte e : diff ) {
    			intdiff += (e & 0xFF);
    		}
    	    return intdiff;
    	}
    	public static byte[] xor(byte lhs[], byte rhs[]) {
            if ((lhs == null) || (rhs == null) || (lhs.length != rhs.length)) return null;
            byte diff[] = new byte[lhs.length];
            xor(lhs, 0, rhs, 0, diff, 0, lhs.length);
            return diff;
        }
        public static void xor(byte lhs[], int startLeft, byte rhs[], int startRight, byte out[], int startOut, int len) {
            if ( (lhs == null) || (rhs == null) || (out == null) )
                throw new NullPointerException("Null params to xor");
            if (lhs.length < startLeft + len)
                throw new IllegalArgumentException("Left hand side is too short");
            if (rhs.length < startRight + len)
                throw new IllegalArgumentException("Right hand side is too short");
            if (out.length < startOut + len)
                throw new IllegalArgumentException("Result is too short");
            
            for (int i = 0; i < len; i++)
                out[startOut + i] = (byte) (lhs[startLeft + i] ^ rhs[startRight + i]);
        }
        public static <T> T JsonToDObject() {
        	
        }
    }
}
