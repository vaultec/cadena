package io.cadena.ip2cs;

import java.util.Map;

public interface PubsubListener {
	public String onReceive(Map<String, Object>  in);
}
