package io.cadena.ip2cs;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import io.ipfs.api.IPFS;
import io.ipfs.multihash.Multihash;

public class PubsubHandler extends Thread {
	private IPFS ipfs;
	//private List<HelloListener> listeners = new ArrayList<HelloListener>();
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private PubsubListener listener;
	
	public PubsubHandler(IPFS ipfs) {
		this.ipfs = ipfs;
	}
	
	public void setListener(PubsubListener e) {
		this.listener = e;
	}
	
	public void run() {
		Iterator<Map<String, Object>> announce;
		
		try {
			announce = ipfs.pubsub.sub("cadena-announce").iterator();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		try {
			System.out.println(this.ipfs.stats.bw());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		for( ; ; ) {
			
			if(announce.hasNext()) {
				Map<String, Object> next = announce.next();
				this.listener.onReceive(next);
			}
			
			try {
				TimeUnit.MILLISECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
