package io.cadena.ip2cs;

import java.util.Base64;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import io.ipfs.api.IPFS;
import io.ipfs.cid.Cid;

public class IP2CS extends Thread implements PubsubListener {
	private PubsubHandler pubsubHandler;
	
	public IP2CS(IPFS ipfs) {
		this.pubsubHandler = new PubsubHandler(ipfs);
		this.pubsubHandler.setListener(this);
		this.pubsubHandler.start();
	}
	
	public String onReceive(Map<String, Object> in) {
		byte[] bytes = Base64.getDecoder().decode(in.get("from").toString().getBytes());
		System.out.println(new String(bytes));
		Cid.cast(bytes);
		System.out.println(Cid.cast(bytes));
		System.out.println(new Gson().toJson(in));
		
		
		return null;
	}
	public static void main(String[] args) {
		new IP2CS(new IPFS("/ip4/127.0.0.1/tcp/5001"));
	}
}
