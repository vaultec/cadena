package io.cadena.exceptions;

public class BlockNotFound extends Exception {
	private static final long serialVersionUID = 8556738665419705951L;
	
	public BlockNotFound(String message) {
		super(message);
	}
	
	public BlockNotFound(String message, Throwable cause) {
		super(message, cause);
	}
}
