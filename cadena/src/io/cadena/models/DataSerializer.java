package io.cadena.models;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

import io.ipfs.multihash.Multihash;

public interface DataSerializer {
	public class MultihashSerializer implements Serializer<BlockHeader>, Serializable {
		private static final long serialVersionUID = 7876061341203527796L;
		
		public void serialize(DataOutput2 out, BlockHeader value) throws IOException {
	    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ObjectOutputStream oos = new ObjectOutputStream( baos );
	        oos.writeObject( value );
	        oos.close();
	        out.writeBytes(baos.toString());
	        //out.writeBytes(new Gson().toJson(value, VideoEntry.class));
	        
	    }
	    public BlockHeader deserialize(DataInput2 in, int available) throws IOException {
	    	byte [] data = null;
	    	in.readFully(data);
	    	System.out.println(new String(data));
	    	System.out.println(new String(in.internalByteArray()));
	        ObjectInputStream ois = new ObjectInputStream( 
	                                        new ByteArrayInputStream(  data ) );
	        Object o = null;
			try {
				o = ois.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        ois.close();
	        return (BlockHeader) o;
	        //return new Gson().fromJson(new String(data), Multihash.class);
	    }
	}
}
