package io.cadena.models;

import io.ipfs.multihash.Multihash;

public class StateBlock {
	protected Multihash previous;
	protected long timestamp;
	protected Multihash[] BlockHeaders;
}
