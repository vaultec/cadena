package io.cadena.models;

import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;

import eu.bittrade.libs.steemj.base.models.Witness;
import io.ipfs.api.IPFS;
import io.ipfs.api.cbor.CborObject;
import io.ipfs.api.cbor.Cborable;
import io.ipfs.multihash.Multihash;

public class BlockHeader implements Cborable {
	
	protected final int block_num;
	protected final long timestamp;
	protected final String witness;
	
	protected final Multihash transactionMerkleRoot;
	protected final Multihash datablock;
	protected final Optional<Multihash> previous;
	
	public BlockHeader(int block_num, long timestamp, String witness, Multihash transactionMerkleRoot, Multihash datablock, Multihash previous) {
		this.block_num = block_num;
		this.timestamp = timestamp;
		this.witness = witness;
		this.transactionMerkleRoot = transactionMerkleRoot;
		this.datablock = datablock;
		if(previous == null) {
			this.previous = Optional.empty();
		} else {
			this.previous = Optional.of(previous);
		}
	}
	/*
	public void setprevious(Multihash previous) {
		this.previous = Optional.of(previous);
	}
	public String getWitness() {
		return witness;
	}
	
	public void setWitness(String witness) {
		this.witness = witness;
	}
	
	public Multihash getTransactionMerkleRoot() {
		return this.transactionMerkleRoot;
	}
	
	public void setTransactionMerkleRoot(Multihash transactionMerkleRoot) {
        this.transactionMerkleRoot = transactionMerkleRoot;
    }*/
	public CborObject toCbor() {
		SortedMap<CborObject, CborObject> objectmap = new TreeMap<CborObject, CborObject>();
		if(this.previous.isPresent()) {
			objectmap.put(new CborObject.CborString("previous"), 
					new CborObject.CborMerkleLink(this.previous.get()));
		} else {
			objectmap.put(new CborObject.CborString("previous"), 
					new CborObject.CborNull());
		}
		objectmap.put(new CborObject.CborString("timestamp"), 
				new CborObject.CborLong(this.timestamp));
		objectmap.put(new CborObject.CborString("witness"), 
				new CborObject.CborString(this.witness));
		objectmap.put(new CborObject.CborString("transactionMerkleRoot"), 
				new CborObject.CborMerkleLink(this.transactionMerkleRoot));
		objectmap.put(new CborObject.CborString("datablock"), 
				new CborObject.CborMerkleLink(this.datablock));
		objectmap.put(new CborObject.CborString("block_num"), 
				new CborObject.CborLong(this.block_num));
		
		CborObject object = new CborObject.CborMap(objectmap);
		return object;
	}
}
