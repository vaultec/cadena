package io.cadena.models;

import java.util.SortedMap;
import java.util.TreeMap;

import io.cadena.Constants.datatype;
import io.ipfs.api.cbor.CborObject;
import io.ipfs.multihash.Multihash;

public class SignedBlockHeader extends BlockHeader {
	
	public SignedBlockHeader(int block_num, long timestamp, String witness, Multihash transactionMerkleRoot,
			Multihash datablock, Multihash previous) {
		super(block_num, timestamp, witness, transactionMerkleRoot, datablock, previous);
	}

	protected byte[] signature;
	datatype type = datatype.signedblockheader;
	
	public CborObject toCbor() {
		SortedMap<CborObject, CborObject> objectmap = new TreeMap<CborObject, CborObject>();
		if(this.previous.isPresent()) {
			objectmap.put(new CborObject.CborString("previous"), 
					new CborObject.CborMerkleLink(this.previous.get()));
		} else {
			objectmap.put(new CborObject.CborString("previous"), 
					new CborObject.CborNull());
		}
		objectmap.put(new CborObject.CborString("timestamp"), 
				new CborObject.CborLong(this.timestamp));
		objectmap.put(new CborObject.CborString("witness"), 
				new CborObject.CborString(this.witness));
		objectmap.put(new CborObject.CborString("transactionMerkleRoot"), 
				new CborObject.CborMerkleLink(this.transactionMerkleRoot));
		objectmap.put(new CborObject.CborString("datablock_hash"), 
				new CborObject.CborMerkleLink(this.datablock));
		objectmap.put(new CborObject.CborString("block_num"), 
				new CborObject.CborLong(this.block_num));
		
		objectmap.put(new CborObject.CborString("signature"), 
				new CborObject.CborByteArray(this.signature));
		
		CborObject object = new CborObject.CborMap(objectmap);
		return object;
	}
}
