package io.cadena.models;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import io.ipfs.api.cbor.CborObject;
import io.ipfs.api.cbor.Cborable;
import io.ipfs.multihash.Multihash;

public class Block implements Cborable {
	private int blockNum;
	private List<Multihash> transactionIds;

	public CborObject toCbor() {
		SortedMap<CborObject, CborObject> objectmap = new TreeMap<CborObject, CborObject>();
		List<CborObject> transactionIds = new ArrayList<CborObject>();
		for(Multihash e : this.transactionIds) {
			transactionIds.add(new CborObject.CborList(transactionIds));
		}
		objectmap.put(new CborObject.CborString("transactionIds"),
				new CborObject.CborList(transactionIds));
		objectmap.put(new CborObject.CborString("block_num"), 
				new CborObject.CborLong(blockNum));
		return new CborObject.CborMap(objectmap);
	}
}
